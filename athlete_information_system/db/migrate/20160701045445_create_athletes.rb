class CreateAthletes < ActiveRecord::Migration
  def change
    create_table :athletes do |t|
      t.string :first_name
      t.string :last_name
      t.integer :training_hours_per_week
      t.string :sports

      t.timestamps null: false
    end
  end
end
