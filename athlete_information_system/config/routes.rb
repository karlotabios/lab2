Rails.application.routes.draw do
  root to: "pages#index"
  get "/athletes", to: "pages#database"
  get "/athletes/add_athlete", to: "pages#add_athlete"
  post "/athletes/create_athlete", to: "pages#create_athlete"
end
