class PagesController < ApplicationController
	def index
		render "pages/index.html.erb"
	end

	def database
		@athlete = Athlete.all

		render "pages/athlete.html.erb"
	end

	def add_athlete
		render "pages/add_athlete.html.erb"
	end

	def create_athlete
		athlete = Athlete.new
		athlete.first_name = params[:first_name]
		athlete.last_name = params[:last_name]
		athlete.training_hours_per_week = params[:training_hours_per_week]
		athlete.sports = params[:sports]
		athlete.save

		redirect_to "/athletes"
	end
end